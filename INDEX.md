# APPEND

APPEND enables programs to open data files in specified directories as if the files were in the current directory.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## APPEND.LSM

<table>
<tr><td>title</td><td>APPEND</td></tr>
<tr><td>version</td><td>5.0-0.6a</td></tr>
<tr><td>entered&nbsp;date</td><td>2006-01-23</td></tr>
<tr><td>description</td><td>APPEND enables programs to open data files in specified directories as if the files were in the current directory.</td></tr>
<tr><td>keywords</td><td>freedos, append, data, files, directories</td></tr>
<tr><td>author</td><td>Eduardo Casino-Almao &lt;mail@eduardocasino.es&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eduardo Casino-Almao &lt;mail@eduardocasino.es&gt;</td></tr>
<tr><td>platforms</td><td>DOS (nasm)</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Append</td></tr>
</table>
